This repository contains example robot code from the 2016 season.
The code is not the actual code used on the 2016 robot for competition.
Instead it was a code base used by team mentors for instructional purposes.
#ifndef SRC_SUSPENSION_H_
#define SRC_SUSPENSION_H_

#include "Solenoid.h"

// Class to represent the robot suspension which is pneumatically controlled with 2 solenoids
class Suspension
{
private:
    // The solenoids controlling the front and rear suspension elements
    Solenoid* pSolenoidFront;
    Solenoid* pSolenoidRear;

public:
    // Constructor
    Suspension();

    // Destructor
    virtual ~Suspension();

    // Raise the suspension (front and rear)
    void Raise();

    // Lower the suspension (front and rear)
    void Lower();

    // Raise only the front
    void RaiseFront();

    // Raise only the rear
    void RaiseRear();

    // Lower only the front
    void LowerFront();

    // Lower only the rear
    void LowerRear();
};

#endif /* SRC_SUSPENSION_H_ */

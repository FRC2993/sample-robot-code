#ifndef _VOLTAGE_MONITOR_H_
#define _VOLTAGE_MONITOR_H_

// Class to monitor battery average voltage level over a given time frame
// with a periodic sample rate.
class VoltageMonitor
{
private:
    // The threshold voltage level, below which Monitor will return false
    float voltageThreshold;

    // The last sample time in msec
    double lastSampleTimeMsec;

    // The sample interval in msec
    double sampleIntervalMsec;

    // The maximum number of samples to store
    int maxSamples;

    // The current number of samples gathered
    int numSamples;

    // The stored samples
    float priorVoltageSample;

    // The calculated average voltage over the sample window
    float averageVoltage;

    // Running sum of voltages over the sample window
    float voltageSum;

    // Gets the current cumulative time in msec
    double GetTimeMsec();

public:
    // Constructor
    // voltageThreshold - the voltage below which Monitor will return false
    // sampleIntervalMsec - the interval in msec between voltage samples
    // sampleWindowMsec - the window of timer over which an average voltage will be calculated
    VoltageMonitor(float voltageThreshold, double sampleIntervalMsec, double sampleWindowMsec);

    // Destructor
    ~VoltageMonitor();

    // Monitor voltage.  Should be called every iteration of AutonomousPeriodic, TeleopPeriodic, etc.
    // returns true if the voltage is below the threshold, false otherwise
    bool Monitor();

    // Gets the currently calculated voltage threshold
    float GetAverageVoltage();
};

#endif


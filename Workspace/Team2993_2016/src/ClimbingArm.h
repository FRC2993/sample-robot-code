#ifndef SRC_CLIMBINGARM_H_
#define SRC_CLIMBINGARM_H_

#include "Joystick.h"
#include "Solenoid.h"
#include "CANTalon.h"

class ClimbingArm
{
private:
    Joystick* pJoystick;
    Solenoid* pSolenoid;
    CANTalon* pTalon1;
    CANTalon* pTalon2;

    bool armReleased;

public:
    ClimbingArm();

    virtual ~ClimbingArm();

    void Update();

    void ReleaseArm();
};

#endif /* SRC_CLIMBINGARM_H_ */

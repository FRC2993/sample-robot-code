#include "Autonomous.h"
#include "Robot.h"

Autonomous::Autonomous(SendableChooser* pChooser, Robot* pRobot)
{
    this->pRobot = pRobot;
    std::string name;

    name = "MoatCrossAndLowGoal2";
    initFuncs[name] = &Autonomous::MoatCrossAndLowGoal2Init;
    periodicFuncs[name] = &Autonomous::MoatCrossAndLowGoal2Periodic;

    name = "MoatCrossAndLowGoal3";
    initFuncs[name] = &Autonomous::MoatCrossAndLowGoal3Init;
    periodicFuncs[name] = &Autonomous::MoatCrossAndLowGoal3Periodic;

    name = "MoatCrossAndLowGoal4";
    initFuncs[name] = &Autonomous::MoatCrossAndLowGoal4Init;
    periodicFuncs[name] = &Autonomous::MoatCrossAndLowGoal4Periodic;

    name = "MoatCrossAndLowGoal5";
    initFuncs[name] = &Autonomous::MoatCrossAndLowGoal5Init;
    periodicFuncs[name] = &Autonomous::MoatCrossAndLowGoal5Periodic;

    for(std::map<std::string, Func>::iterator i = initFuncs.begin();
         i != initFuncs.end();
         ++i)
    {
        std::string name = i->first;
        if (i == initFuncs.begin())
        {
            pChooser->AddDefault(name, (void*)&name);
        }
        else
        {
            pChooser->AddObject(name, (void*)&name);
        }
    }

    SmartDashboard::PutData("Auto Modes", pChooser);
}

Autonomous::~Autonomous()
{
}

void Autonomous::RunInit(std::string name)
{
    (this->*initFuncs[name])();
}

void Autonomous::RunPeriodic(std::string name)
{
    (this->*periodicFuncs[name])();

}

void Autonomous::MoatCrossAndLowGoal2Init()
{
    // AutonomousInit function for moat crossing and low goal from position 2
}

void Autonomous::MoatCrossAndLowGoal2Periodic()
{
    // AutonomousPeriodic function for moat crossing and low goal from position 2
}


void Autonomous::MoatCrossAndLowGoal3Init()
{
    // AutonomousInit function for moat crossing and low goal from position 3
}

void Autonomous::MoatCrossAndLowGoal3Periodic()
{
    // AutonomousPeriodic function for moat crossing and low goal from position 3
}


void Autonomous::MoatCrossAndLowGoal4Init()
{
    // AutonomousInit function for moat crossing and low goal from position 4
}

void Autonomous::MoatCrossAndLowGoal4Periodic()
{
    // AutonomousPeriodic function for moat crossing and low goal from position 4
}


void Autonomous::MoatCrossAndLowGoal5Init()
{
    // AutonomousInit function for moat crossing and low goal from position 5
}

void Autonomous::MoatCrossAndLowGoal5Periodic()
{
    // AutonomousPeriodic function for moat crossing and low goal from position 5
}



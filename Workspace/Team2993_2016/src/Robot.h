#ifndef _INCLUDED_ROBOT_H_
#define _INCLUDED_ROBOT_H_

#include "WPILib.h"
#include "Suspension.h"
#include "VoltageMonitor.h"
#include "ClimbingArm.h"
#include "Claw.h"
#include "Drive.h"
#include "Autonomous.h"

class Robot: public IterativeRobot
{
public:
    LiveWindow *lw = LiveWindow::GetInstance();
    SendableChooser *pChooser;
    const std::string autoNameDefault = "Default";
    const std::string autoNameCustom = "My Auto";
    std::string autoSelected;

    Autonomous* pAutonomous;
    Drive* pDrive;
    Suspension* pSuspension;
    VoltageMonitor* pVoltageMonitor;
    Compressor* pCompressor;
    ClimbingArm* pClimbingArm;
    Claw* pClaw;

    // Check for 11.5V threshold, every 250 msec, with averages calculated over 5sec window
    const float batteryThresholdVoltage = 11.5; // volts
    const int batterySampleInterval = 250; // msec
    const int batteryAverageWindow = 5000; // msec

    void RobotInit();
    void AutonomousInit();
    void AutonomousPeriodic();
    void TeleopInit();
    void TeleopPeriodic();
    void TestInit();
    void TestPeriodic();
    void DisabledInit();
    void DisabledPeriodic();
    void VoltageCheck();
};

#endif
